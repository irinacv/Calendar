package com.example.irinacubillovargas.calendar.Class;

/**
 * Created by Irina Cubillo Vargas on 5/5/2018.
 */

public class Document {
    private int id_document;
    private String document;
    private int id_calendar_group_date;
    private String username_creatorID;

    public Document(int id_document, String document, int id_calendar_group_date, String username_creatorID) {
        this.id_document = id_document;
        this.document = document;
        this.id_calendar_group_date = id_calendar_group_date;
        this.username_creatorID = username_creatorID;
    }

    public int getId_document() {
        return id_document;
    }

    public void setId_document(int id_document) {
        this.id_document = id_document;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public int getId_calendar_group_date() {
        return id_calendar_group_date;
    }

    public void setId_calendar_group_date(int id_calendar_group_date) {
        this.id_calendar_group_date = id_calendar_group_date;
    }

    public String getUsername_creatorID() {
        return username_creatorID;
    }

    public void setUsername_creatorID(String username_creatorID) {
        this.username_creatorID = username_creatorID;
    }
}
