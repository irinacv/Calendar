package com.example.irinacubillovargas.calendar.GUI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.Class.My_groups;
import com.example.irinacubillovargas.calendar.Class.User;
import com.example.irinacubillovargas.calendar.Conexion.IP;
import com.example.irinacubillovargas.calendar.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InformacionGrupos extends AppCompatActivity {
    private IP conexionIP = new IP();
    private String nombreGrupo;
    private ArrayList<User> miembros = new ArrayList<>();
    private ArrayList<My_groups> my_groups = new ArrayList<>();
    private AdapterUsuarios adapter;
    private ListView miembrosGroup;
    private TextView name;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion_grupos);
        my_groups.addAll(Globales.MisGrupos);

        Intent intent = getIntent();
        Bundle extra = intent.getExtras();
        if (extra != null) {
            nombreGrupo = extra.getString("nombreGrupo");
        }

        name = (TextView) findViewById(R.id.nombreDelGrupo);
        name.setText(nombreGrupo);

        for (My_groups item : my_groups) {
            if (item.getGroup_name().equals(nombreGrupo)) {
                id = item.getId_group();
            }


        }
        cargarMiembros(id);
    }

    private void ver(){
        adapter = new AdapterUsuarios(this, miembros);
        miembrosGroup = (ListView) findViewById(R.id.ListGroupUsers);
        miembrosGroup.setAdapter(adapter);
    }

    private void cargarMiembros(int id){
        try{
            final Call<List<String>> misGrupos=  conexionIP.getServidor().obtenerMiembrosGrupo(id);
            misGrupos.enqueue(new Callback<List<String>>() {
                @Override
                public void onResponse(Call<List<String>>call, Response<List<String>>response) {
                    List<String> lista = response.body();

                    for (String item : lista){
                        if (!item.equals(Globales.usuarioLogueado)) {
                            User user = new User(item, "123");
                            miembros.add(user);
                        }
                    }
                    ver();
                }
                @Override
                public void onFailure(Call<List<String>> call, Throwable t) {
                }
            });
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Error de conexión", Toast.LENGTH_LONG).show();
        }
    }
}
