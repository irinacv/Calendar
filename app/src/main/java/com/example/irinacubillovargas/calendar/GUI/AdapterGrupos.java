package com.example.irinacubillovargas.calendar.GUI;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.irinacubillovargas.calendar.R;

import java.util.ArrayList;

/**
 * Created by Irina Cubillo Vargas on 22/5/2018.
 */

public class AdapterGrupos extends BaseAdapter {
    protected Activity activity;
    protected ArrayList<String> items;

    public AdapterGrupos (Activity activity, ArrayList<String> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_group, null);
        }

        String dir = items.get(position);

        TextView title = (TextView) v.findViewById(R.id.nameGroup);
        title.setText(dir);

        return v;
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<String> evento) {
        for (int i = 0; i < evento.size(); i++) {
            items.add(evento.get(i));
        }
    }
}