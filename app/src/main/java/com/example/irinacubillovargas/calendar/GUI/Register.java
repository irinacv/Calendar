package com.example.irinacubillovargas.calendar.GUI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.Class.User;
import com.example.irinacubillovargas.calendar.Conexion.IP;
import com.example.irinacubillovargas.calendar.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {

    private IP conexionIP = new IP();
    EditText nombreU, passU;
    String username, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        nombreU= (EditText)findViewById(R.id.registro_usuario);
        passU=(EditText)findViewById(R.id.registro_contrasena);
        Button reg = (Button) findViewById(R.id.registro_boton_crear);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crearObjetoUsuario();
                Intent nuevo = new Intent(getApplicationContext(),Login.class);
                startActivity(nuevo);
                finish();
            }
        });

    }

    public void crearObjetoUsuario(){
        username= nombreU.getText().toString().trim();
        pass= passU.getText().toString().trim();
        User nuevoUsuario= new User(username,pass);

        //insertarUsuarioEnBase(nuevoUsuario);


        if(insertarUsuarioEnBase(nuevoUsuario)){
            Globales.listaUsuarios.add(nuevoUsuario);

        }
    }

    public boolean insertarUsuarioEnBase(User usuario){
        try{
            Call<Boolean> insertar=  conexionIP.getServidor().insertarUsuario(usuario);
            insertar.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    Toast.makeText(getApplicationContext(),"Se ha insertado correctamente", Toast.LENGTH_LONG).show();
                }
                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),"Error nombre de usuario repetido", Toast.LENGTH_LONG).show();
                }
            });
            return true;
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Error de conexión", Toast.LENGTH_LONG).show();
            return false;
        }
    }
}
