package com.example.irinacubillovargas.calendar.Class;

import java.util.Date;

/**
 * Created by Irina Cubillo Vargas on 5/5/2018.
 */

public class Calendar_group_date {
    private int id_calendar_group_date;
    private int id_group;
    private String calendar_date;

    public Calendar_group_date(int id_calendar_group_date, int id_group, String calendar_date) {
        this.id_calendar_group_date = id_calendar_group_date;
        this.id_group = id_group;
        this.calendar_date = calendar_date;
    }

    public int getId_calendar_group_date() {
        return id_calendar_group_date;
    }

    public void setId_calendar_group_date(int id_calendar_group_date) {
        this.id_calendar_group_date = id_calendar_group_date;
    }

    public int getId_group() {
        return id_group;
    }

    public void setId_group(int id_group) {
        this.id_group = id_group;
    }

    public String getCalendar_date() {
        return calendar_date;
    }

    public void setCalendar_date(String calendar_date) {
        this.calendar_date = calendar_date;
    }
}
