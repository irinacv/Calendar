package com.example.irinacubillovargas.calendar.GUI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.R;

import java.util.ArrayList;

public class VerGrupos extends AppCompatActivity {
    private ArrayList<String> misGrupos = new ArrayList<>();
    private AdapterGrupos adapter;
    private ListView listaGrupos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_grupos);
        cargarGrupos();
    }

    private void cargarGrupos(){

        misGrupos.addAll(Globales.getNombreMisGrupos());
        adapter = new AdapterGrupos(this, misGrupos);
        listaGrupos = (ListView) findViewById(R.id.ListGroup);
        listaGrupos.setAdapter(adapter);
        listaGrupos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String grupo = misGrupos.get(position);
                Intent intent = new Intent(VerGrupos.this,InformacionGrupos.class);
                intent.putExtra("nombreGrupo", grupo);
                startActivity(intent);

            }
        });
    }
}
