package com.example.irinacubillovargas.calendar.Class;

/**
 * Created by Irina Cubillo Vargas on 5/5/2018.
 */

public class Message {
    private int id_message;
    private String message_body;
    private int id_calendar_group_date;
    private String username_creatorID;

    public Message(int id_message, String message_body, int id_calendar_group_date, String username_creatorID) {
        this.id_message = id_message;
        this.message_body = message_body;
        this.id_calendar_group_date = id_calendar_group_date;
        this.username_creatorID = username_creatorID;
    }

    public int getId_message() {
        return id_message;
    }

    public void setId_message(int id_message) {
        this.id_message = id_message;
    }

    public String getMessage_body() {
        return message_body;
    }

    public void setMessage_body(String message_body) {
        this.message_body = message_body;
    }

    public int getId_calendar_group_date() {
        return id_calendar_group_date;
    }

    public void setId_calendar_group_date(int id_calendar_group_date) {
        this.id_calendar_group_date = id_calendar_group_date;
    }

    public String getUsername_creatorID() {
        return username_creatorID;
    }

    public void setUsername_creatorID(String username_creatorID) {
        this.username_creatorID = username_creatorID;
    }
}
