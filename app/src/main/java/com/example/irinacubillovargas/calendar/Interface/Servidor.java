package com.example.irinacubillovargas.calendar.Interface;
import com.example.irinacubillovargas.calendar.Class.Calendar_group_date;
import com.example.irinacubillovargas.calendar.Class.Events;
import com.example.irinacubillovargas.calendar.Class.Gallery;
import com.example.irinacubillovargas.calendar.Class.Message;
import com.example.irinacubillovargas.calendar.Class.My_groups;
import com.example.irinacubillovargas.calendar.Class.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
/**
 * Created by Irina Cubillo Vargas on 5/5/2018.
 */

public interface Servidor {


    ///////////////////////////////////////////////////////////////////////////////////////////////

                                    //GETS

    ///////////////////////////////////////////////////////////////////////////////////////////////

    //Obtiene a toda la lista de usuarios del sistema
    @GET("user/select")
    Call<List<User>> obtenerListaDeUsuarios();


    //obtiene todos las actividades relacionadas a un usuario durante en relación a su calendario osea
    //obtiene todas las actividades programadas que el usuario tiene con sus distintos grupos
    @GET("calendarDate/select/{username}")
    Call<List<Calendar_group_date>> obtenerActividadesCalendario(@Path("username") String username);


    //selecciona todos los grupos a los cuales estoy asociado
    @GET("group/my/select/{username}")
    Call<List<My_groups>> obtenerMisGrupos(@Path("username") String username);


    //selecciona a todos los miembros que pertenecen a un grupo determinado
    @GET("group/team/select/{idGroup}")
    Call<List<String>> obtenerMiembrosGrupo(@Path("idGroup") int idGroup);


    //Obtener todos mis eventos de una fecha determinada
    @GET("event/select/{idCalendarDate}")
    Call<List<Events>> obtenerEventosSegunFecha(@Path("idCalendarDate") int idCalendarDate);


    //Obtener todos mis mensajes
    @GET("message/select/{idCalendarDate}")
    Call<List<Message>> obtenerMensajeSegunFecha(@Path("idCalendarDate") int idCalendarDate);

    //Obtener todos mis imagenes
    @GET("gallery/select/{idCalendarDate}")
    Call<List<Gallery>> selectGallery(@Path("idCalendarDate") int idCalendarDate);

    ///////////////////////////////////////////////////////////////////////////////////////////////

                                    //INSERTS

    ///////////////////////////////////////////////////////////////////////////////////////////////


    //inserta usuario
    @POST("user/insert")
    Call<Boolean> insertarUsuario(@Body User usuario);

    //inserta un grupo
    @POST("group/insert/{group_name}")
    Call<Integer> insertarGrupo(@Path("group_name") String group_name);

    //hace la vinculacion del grupo creado con la persona
    @POST("group/my/insert")
    Call<Boolean> insertarMiGrupoo(@Body My_groups grupo);

    //inserta un evento
    @POST("event/insert")
    Call<Boolean> insertarEvento(@Body Events evento);


    //inserta una nueva actividad realizada por un grupo esto es utilizada ya que es la tabla que une
    //al evento, foto, etc con las personas que están asociadas al grupo. Además es importante para
    //conocer la fecha determinada en la cual se realizó la actividad.
    @POST("calendar/insert")
    Call<Integer> insertCalendarGroupDate(@Body Calendar_group_date actividad);

    @POST("message/insert")
    Call<Boolean> insertMensaje(@Body Message message);

    @POST("gallery/insert")
    Call<Boolean> insertGallery(@Body Gallery gallery);

}