package com.example.irinacubillovargas.calendar.Class;

/**
 * Created by Irina Cubillo Vargas on 5/5/2018.
 */

public class My_groups {
    private int id_group;
    private String username_associated;
    private String username_creatorID;
    private String group_name;

    public My_groups(int id_group, String username_associated, String username_creatorID, String group_name) {
        this.id_group = id_group;
        this.username_associated = username_associated;
        this.username_creatorID = username_creatorID;
        this.group_name = group_name;
    }

    public int getId_group() {
        return id_group;
    }

    public void setId_group(int id_group) {
        this.id_group = id_group;
    }

    public String getUsername_associated() {
        return username_associated;
    }

    public void setUsername_associated(String username_associated) {
        this.username_associated = username_associated;
    }

    public String getUsername_creatorID() {
        return username_creatorID;
    }

    public void setUsername_creatorID(String username_creatorID) {
        this.username_creatorID = username_creatorID;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
}
