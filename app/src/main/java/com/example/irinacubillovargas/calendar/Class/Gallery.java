package com.example.irinacubillovargas.calendar.Class;

/**
 * Created by Irina Cubillo Vargas on 5/5/2018.
 */

public class Gallery {
    private int id_picture;
    private String name;
    private String picture;
    private int id_calendar_group_date;
    private String username_creator;

    public Gallery(){

    }

    public Gallery(int id_picture, String name, String picture, int id_calendar_group_date, String username_creator) {
        this.id_picture = id_picture;
        this.name = name;
        this.picture = picture;
        this.id_calendar_group_date = id_calendar_group_date;
        this.username_creator = username_creator;
    }

    public int getId_picture() {
        return id_picture;
    }

    public void setId_picture(int id_picture) {
        this.id_picture = id_picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId_calendar_group_date() {
        return id_calendar_group_date;
    }

    public void setId_calendar_group_date(int id_calendar_group_date) {
        this.id_calendar_group_date = id_calendar_group_date;
    }

    public String getUsername_creator() {
        return username_creator;
    }

    public void setUsername_creator(String username_creator) {
        this.username_creator = username_creator;
    }
}
