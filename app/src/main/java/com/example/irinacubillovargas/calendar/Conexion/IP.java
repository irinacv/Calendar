package com.example.irinacubillovargas.calendar.Conexion;

import com.example.irinacubillovargas.calendar.Interface.Servidor;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class IP {
    private String baseurl;
    private final Retrofit retrofit;
    private Servidor servidor;

    public IP() {
        this.baseurl = "http://172.26.46.33:8090";
        this.retrofit = new Retrofit.Builder().baseUrl(baseurl).addConverterFactory(GsonConverterFactory.create()).build();
        this.servidor = retrofit.create(Servidor.class);
    }
    public Servidor getServidor() {
        return this.servidor;
    }
}
