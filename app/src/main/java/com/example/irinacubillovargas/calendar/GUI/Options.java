package com.example.irinacubillovargas.calendar.GUI;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.irinacubillovargas.calendar.Class.Calendar_group_date;
import com.example.irinacubillovargas.calendar.Class.Events;
import com.example.irinacubillovargas.calendar.Class.Gallery;
import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.Class.Message;
import com.example.irinacubillovargas.calendar.Class.My_groups;
import com.example.irinacubillovargas.calendar.Conexion.IP;
import com.example.irinacubillovargas.calendar.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Options extends AppCompatActivity {
    private static final int REQUEST_CODE = 1234;
    private IP conexionIP = new IP();
    ArrayList<Events> category = new ArrayList<Events>();
    ArrayList<Message>listaMensajes= new ArrayList<>();
    ListView listViewEventos, listViewMensajes;
    EditText grupo, descripcionmensajeCuerpo;
    TabHost tabs;
    ArrayList<String> matchesText;
    GridView gridViewArchivo;
    DatabaseReference databaseReference;
    ArrayList<String> letra = new ArrayList<>();
    Spinner grupoSpinner;
    String descripcionMensaje,grupoMensaje;
    Button botonAgregarEvento, agreagrMsj, botonArchivos;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        databaseReference= FirebaseDatabase.getInstance().getReference("gallery"); //tabla users
        gridViewArchivo= (GridView)findViewById(R.id.gridview);
        obtenerTodosLasGallery();

        grupoSpinner = (Spinner) findViewById(R.id.crearMensaje_spinnerGrupo);
        grupo=(EditText)findViewById(R.id.crearMensaje_grupo);
        descripcionmensajeCuerpo=(EditText)findViewById(R.id.input_mensaje_cuerpo);
        listViewMensajes= (ListView)findViewById(R.id.listViewlistaMensajes);
        listViewEventos= (ListView) findViewById(R.id.listViewlistaEventos);
        tabs=(TabHost)findViewById(android.R.id.tabhost);
        agreagrMsj= (Button)findViewById(R.id.botonAgregarMensaje);
        botonArchivos= (Button)findViewById(R.id.botonAgregarGallery);
        descripcionmensajeCuerpo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE , "es-ES");
                startActivityForResult(intent, REQUEST_CODE);
                return false;
            }
        });

        agreagrMsj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agregarMensajeOpciones();
            }
        });
        botonAgregarEvento= (Button)findViewById(R.id.botonAgregarEvento);
        botonAgregarEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ventaneNueva= new Intent(getApplicationContext(),CrearEventoActivity.class);
                startActivity(ventaneNueva);
            }
        });
        obtenerTodosLosEventos();
        obtenerTodosLosMensajes();
        obtenerMisGrupos();
        insertarTabHost();

        grupoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                grupo.setText(letra.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        botonArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ventaneNueva= new Intent(getApplicationContext(),crearArchivoActivity.class);
                startActivity(ventaneNueva);
            }
        });
    }


    public void agregarMensajeOpciones(){
        descripcionMensaje= descripcionmensajeCuerpo.getText().toString();
        grupoMensaje=grupo.getText().toString();

        int posicionCal= -1;
        int idGrupo= -1;

        if(Globales.listaIdCalendarioFechaEspecif.size() == 0){
            for(int j= 0; j< Globales.MisGrupos.size(); j++){
                if(Globales.MisGrupos.get(j).getGroup_name().equals(grupoMensaje)){
                    idGrupo= Globales.MisGrupos.get(j).getId_group();
                }
            }
        }

        for (int i= 0; i< Globales.listaIdCalendarioFechaEspecif.size(); i++){
            for(int j= 0; j< Globales.MisGrupos.size(); j++){
                if(Globales.MisGrupos.get(j).getGroup_name().equals(grupoMensaje)){
                    idGrupo= Globales.MisGrupos.get(j).getId_group();
                }
                if(Globales.listaIdCalendarioFechaEspecif.get(i).getId_group() == Globales.MisGrupos.get(j).getId_group() &&
                        Globales.MisGrupos.get(j).getGroup_name().equals(grupoMensaje)){
                    posicionCal= i;
                    break;
                }
            }
        }

        if(posicionCal == -1){
            String fehcaCrea= Globales.fechaGlobal;
            Log.d("dasdas 22", "dsadasdasdas");
            Calendar_group_date nuevo= new Calendar_group_date(0,idGrupo, Globales.fechaGlobal);
            insertarCalendarGroup(nuevo);
        }
        else{
            Log.d("dasdas 22", String.valueOf(idGrupo));
            Log.d("dasdas ee", String.valueOf(posicionCal));

            Message msjnuevo= new Message(0,descripcionMensaje,
                    Globales.listaIdCalendarioFechaEspecif.get(posicionCal).getId_calendar_group_date(),Globales.getUsuarioLogueado());
            insertarEnBDMesaje(msjnuevo);
        }
    }

    public void insertarCalendarGroup(Calendar_group_date nuevo){
        Log.d("fecha",nuevo.getCalendar_date());
        Log.d("calendar gropu date",String.valueOf(nuevo.getId_calendar_group_date()));
        Log.d("id grupo",String.valueOf(nuevo.getId_group()));

        Call<Integer> calendarioActividades=  conexionIP.getServidor().insertCalendarGroupDate(nuevo);
        calendarioActividades.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                String respuesta= response.message();
                Log.d("mensaje",respuesta);
                int idGrupo= response.body();
                Message msjnuevo= new Message(0,descripcionMensaje,
                        idGrupo,Globales.getUsuarioLogueado());
                insertarEnBDMesaje(msjnuevo);
            }
            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });
    }

    public void insertarEnBDMesaje(Message mensaje){
        Call<Boolean> calendarioActividades=  conexionIP.getServidor().insertMensaje(mensaje);
        calendarioActividades.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.d("cacacs",response.message());
                if(response.body().equals(true)){
                    Toast.makeText(getApplicationContext(), "Se ha creado el mensaje exitosamente", Toast.LENGTH_SHORT).show();
                    Intent nuevo = new Intent(getApplicationContext(), com.example.irinacubillovargas.calendar.GUI.Calendar.class);
                    startActivity(nuevo);
                    finish();
                }
                else{
                    Toast.makeText(getApplicationContext(), "ERROR: no se ha creado mensaje", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

    public class Adapter extends BaseAdapter {

        LayoutInflater myInflater;

        public Adapter(Context context){
            myInflater= LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return category.size();
        }

        @Override
        public Object getItem(int position) {
            return category.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView== null){
                convertView= myInflater.inflate(R.layout.lista_actividades_layout,null);
            }
            TextView title = (TextView) convertView.findViewById(R.id.eventoTituloEvento);
            title.setText(category.get(position).getName());

            TextView description = (TextView) convertView.findViewById(R.id.eventoDescripcionEvento);
            description.setText(category.get(position).getDescription_event());


            return convertView;
        }
    }

    public class AdapterMsj extends BaseAdapter {

        LayoutInflater myInflater;

        public AdapterMsj(Context context){
            myInflater= LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return listaMensajes.size();
        }

        @Override
        public Object getItem(int position) {
            return listaMensajes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView== null){
                convertView= myInflater.inflate(R.layout.lista_mensajes,null);
            }
            TextView title = (TextView) convertView.findViewById(R.id.mensajeNombre);
            title.setText(listaMensajes.get(position).getUsername_creatorID());

            TextView description = (TextView) convertView.findViewById(R.id.mensajeDescripcion);
            description.setText(listaMensajes.get(position).getMessage_body());

            return convertView;
        }
    }

    ArrayList<Gallery>listaGalería= new ArrayList<>();
    public void obtenerTodosLasGallery(){
        try{
            listaGalería.clear();
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (int i= 0; i<Globales.listaIdCalendarioFechaEspecif.size(); i++){
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            Gallery gallery = postSnapshot.getValue(Gallery.class);
                            if (gallery.getId_calendar_group_date() ==
                                    Globales.listaIdCalendarioFechaEspecif.get(i).getId_calendar_group_date()){
                                listaGalería.add(gallery);
                            }
                        }
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            gridViewArchivo.setAdapter(new AdapterGallery(getApplicationContext(), listaGalería));
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Eror de conexión", Toast.LENGTH_LONG).show();
        }
    }

    public void obtenerMisGrupos(){
        letra.addAll(Globales.nombreMisGrupos);
        ArrayAdapter adaptador= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, letra);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        grupoSpinner.setAdapter(adaptador);
    }
    public void obtenerTodosLosMensajes(){
        try{
            listaMensajes.clear();
            for (int i= 0; i<Globales.listaIdCalendarioFechaEspecif.size(); i++){
                Call<List<Message>> calendarioActividades=  conexionIP.getServidor().obtenerMensajeSegunFecha(Globales.listaIdCalendarioFechaEspecif.get(i).getId_calendar_group_date());
                calendarioActividades.enqueue(new Callback<List<Message>>() {
                    @Override
                    public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                        List<Message>lista= response.body();
                        listaMensajes.addAll(lista);
                        Log.d("inserto 2", String.valueOf(lista.size()));
                        //Toast.makeText(Options.this, "Usted posee: "+ String.valueOf(category.size())+ " eventos", Toast.LENGTH_SHORT).show();
                        listViewMensajes.setAdapter(new AdapterMsj(getApplicationContext()));
                    }
                    @Override
                    public void onFailure(Call<List<Message>> call, Throwable t) {

                    }
                });
            }
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Eror de conexión", Toast.LENGTH_LONG).show();
        }
    }

    public void obtenerTodosLosEventos(){
        try{
            category.clear();
            for (int i= 0; i<Globales.listaIdCalendarioFechaEspecif.size(); i++){
                Call<List<Events>> calendarioActividades=  conexionIP.getServidor().obtenerEventosSegunFecha(Globales.listaIdCalendarioFechaEspecif.get(i).getId_calendar_group_date());
                calendarioActividades.enqueue(new Callback<List<Events>>() {
                    @Override
                    public void onResponse(Call<List<Events>> call, Response<List<Events>> response) {
                        List<Events>lista= response.body();
                        category.addAll(lista);
                        Log.d("inserto 2", String.valueOf(lista.size()));
                        //Toast.makeText(Options.this, "Usted posee: "+ String.valueOf(category.size())+ " eventos", Toast.LENGTH_SHORT).show();
                        listViewEventos.setAdapter(new Adapter(getApplicationContext()));
                    }
                    @Override
                    public void onFailure(Call<List<Events>> call, Throwable t) {

                    }
                });
            }
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Eror de conexión", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //obtenerTodosLosEventos();
    }

    public void insertarTabHost(){
        tabs.setup();
        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setIndicator("Eventos");
        spec.setContent(R.id.tab1);
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab2");
        spec.setIndicator("Archivos");
        spec.setContent(R.id.tab2);
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab3");
        spec.setIndicator("Mensajes");
        spec.setContent(R.id.tab3);
        tabs.addTab(spec);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            String palabra = "";
            matchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(matchesText.size() != 0){
                palabra = matchesText.get(0);
            }
            descripcionmensajeCuerpo.setText(palabra);
        }
    }
}