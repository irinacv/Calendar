package com.example.irinacubillovargas.calendar.GUI;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.Class.My_groups;
import com.example.irinacubillovargas.calendar.Class.User;
import com.example.irinacubillovargas.calendar.Conexion.IP;
import com.example.irinacubillovargas.calendar.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrearGrupo extends AppCompatActivity {
    private IP conexionIP = new IP();
    private Button aceptar, buscar;
    private ArrayList<User> misUsers = new ArrayList<>();
    private ArrayList<String> misGrupos = new ArrayList<>();
    private AdapterUsuarios adapter;
    private ArrayList<My_groups> grupoTemp = new ArrayList<>();
    private ListView listaUsers;

    EditText nombreGrupo, editUser;
    String nombreG, usuario;
    ArrayList<String> matchesText;

    Boolean flag = true;

    private static final int REQUEST_CODE = 1234;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_grupo);

        aceptar = (Button)findViewById(R.id.aceptar);
        nombreGrupo = (EditText) findViewById(R.id.nombreGrupo);
        buscar = (Button) findViewById(R.id.buscar);

        misUsers.addAll(Globales.getListaUsuarios());
        misGrupos.addAll(Globales.getNombreMisGrupos());

        editUser = (EditText) findViewById(R.id.usuario);
        editUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isConnected()){
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    //intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-Es");
                    startActivityForResult(intent, REQUEST_CODE);
                } else {
                    Toast.makeText(CrearGrupo.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        });

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nombreG = nombreGrupo.getText().toString();
                if(nombreG.equals("") || grupoTemp.size()==0){
                    Toast.makeText(getApplicationContext(), "Llenar todos los espacios", Toast.LENGTH_SHORT).show();
                }
                else {
                    flag = buscarGrupo(nombreG);
                    if(flag){
                        alertaRepetido();
                    }
                    else {
                        Globales.nombreMisGrupos.add(nombreG);
                        insertarGroup(nombreG);
                        //Toast.makeText(getApplicationContext(), "Hola", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = buscarUser(usuario);
                if(flag){
                    avisoDeBusquedaUser(usuario);
                }
                else {
                    alertaNoExiste();
                }
            }
        });
        cargarUsers();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE){
            super.onActivityResult(requestCode, resultCode, data);
            matchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS); //Get data of data
            editUser.setText(matchesText.get(0));
            usuario = editUser.getText().toString();
        }
    }

    public boolean isConnected(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!= null && net.isAvailable() && net.isConnected()){
            return true;
        }   else {
            return false;
        }
    }

    private void alertaNoExiste(){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Alerta");
        dialogo1.setMessage("El usuario no existe");
        dialogo1.setCancelable(false);
        dialogo1.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.cancel();
            }
        });
        dialogo1.show();
    }

    private void alertaRepetido(){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Alerta");
        dialogo1.setMessage("Este nombre ya fue agregado");
        dialogo1.setCancelable(false);
        dialogo1.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.cancel();
            }
        });
        dialogo1.show();
    }

    private void avisoDeBusquedaUser(final String usuario){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage("El usuario existe,¿Desea agregarlo?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                flag = verificarUser(usuario);
                if (flag){
                    alertaRepetido();
                }
                else {
                    My_groups my_groups = new My_groups(-1, usuario, Globales.usuarioLogueado, "");
                    grupoTemp.add(my_groups);
                }
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.cancel();
            }
        });
        dialogo1.show();

    }

    private boolean verificarUser(String user){
        for (My_groups item : grupoTemp){
            if (item.getUsername_associated().equals(user)){
                return true;
            }
        }
        return false;
    }

    private boolean buscarUser(String user){
        for (User item : misUsers){
            if (item.getUsername().equals(user)){
                return true;
            }
        }
        return false;
    }

    private boolean buscarGrupo(String grupo){
        for (String item : misGrupos){
            if (item.equals(grupo)){
                return true;
            }
        }
        return false;
    }

    private void agregar(){

    }

    private void cargarUsers(){
        adapter = new AdapterUsuarios(this,misUsers);
        listaUsers = (ListView) findViewById(R.id.ListUser);
        listaUsers.setAdapter(adapter);
        listaUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = misUsers.get(position);
                flag = verificarUser(user.getUsername());
                if (flag){
                    alertaRepetido();
                }
                else {
                    My_groups my_groups = new My_groups(-1,user.getUsername(),Globales.usuarioLogueado,"");
                    grupoTemp.add(my_groups);
                    //Toast.makeText(getApplicationContext(), grupoTemp.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void insertarGroup(final String nombreGrupo){
        Call<Integer> calendarioActividades=  conexionIP.getServidor().insertarGrupo(nombreGrupo);
        calendarioActividades.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                String respuesta= response.message();
                Log.d("mensaje",respuesta);
                int idGrupo = response.body();

                My_groups my_groups = new My_groups(idGrupo,Globales.usuarioLogueado,Globales.usuarioLogueado,nombreGrupo);
                insertarEnBDMy_group(my_groups);

                Toast.makeText(getApplicationContext(), Integer.toString(idGrupo), Toast.LENGTH_SHORT).show();
                for (My_groups item: grupoTemp){
                    item.setId_group(idGrupo);
                    item.setGroup_name(nombreGrupo);
                    insertarEnBDMy_group(item);
                }
            }
            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });
    }

    public void insertarEnBDMy_group(My_groups my_groups){
        Call<Boolean> insertMy_group =  conexionIP.getServidor().insertarMiGrupoo(my_groups);
        insertMy_group.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.body().equals(true)){
                    Toast.makeText(CrearGrupo.this, "Se ha creado el My_group exitosamente", Toast.LENGTH_SHORT).show();
                    Intent nuevo = new Intent(CrearGrupo.this, Calendar.class);
                    startActivity(nuevo);
                }
                else{
                    Toast.makeText(CrearGrupo.this, "Error: no se ha creado el My_group", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(CrearGrupo.this, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
