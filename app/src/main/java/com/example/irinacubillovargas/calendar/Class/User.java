package com.example.irinacubillovargas.calendar.Class;

/**
 * Created by Irina Cubillo Vargas on 5/5/2018.
 */

public class User {
    private String username;
    private String userpass;

    public User(String username, String userpass) {
        this.username = username;
        this.userpass = userpass;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass;
    }
}
