package com.example.irinacubillovargas.calendar.Class;

/**
 * Created by Irina Cubillo Vargas on 5/5/2018.
 */

public class Events {
    private int id_event;
    private String name;
    private String description_event;
    private String time_event;
    private int id_calendar_group_date;
    private String username_creatorID;

    public Events(int id_event, String name, String description_event, String time_event, int id_calendar_group_date, String username_creatorID) {
        this.id_event = id_event;
        this.name = name;
        this.description_event = description_event;
        this.time_event = time_event;
        this.id_calendar_group_date = id_calendar_group_date;
        this.username_creatorID = username_creatorID;
    }

    public int getId_event() {
        return id_event;
    }

    public void setId_event(int id_event) {
        this.id_event = id_event;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription_event() {
        return description_event;
    }

    public void setDescription_event(String description_event) {
        this.description_event = description_event;
    }

    public String getTime_event() {
        return time_event;
    }

    public void setTime_event(String time_event) {
        this.time_event = time_event;
    }

    public int getId_calendar_group_date() {
        return id_calendar_group_date;
    }

    public void setId_calendar_group_date(int id_calendar_group_date) {
        this.id_calendar_group_date = id_calendar_group_date;
    }

    public String getUsername_creatorID() {
        return username_creatorID;
    }

    public void setUsername_creatorID(String username_creatorID) {
        this.username_creatorID = username_creatorID;
    }
}
