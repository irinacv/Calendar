package com.example.irinacubillovargas.calendar.GUI;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.irinacubillovargas.calendar.Class.Calendar_group_date;
import com.example.irinacubillovargas.calendar.Class.Events;
import com.example.irinacubillovargas.calendar.Class.Gallery;
import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.Conexion.IP;
import com.example.irinacubillovargas.calendar.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class crearArchivoActivity extends AppCompatActivity {

    Button botonAceptar,botonAgregarImagen;
    EditText nombreEDT,grupoEDT;
    ImageView imagenCargada_imageView;
    Spinner grupoSpinner;
    ArrayList<String> letra = new ArrayList<>();
    private static final int PICK_IMAGE = 100;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    Uri imageUri;
    private IP conexionIP = new IP();
    Bitmap bitmapIMG;
    DatabaseReference databaseReference;
    String pictureString,grupoString,nombreString;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_archivo);
        vinvularElementos();
        databaseReference= FirebaseDatabase.getInstance().getReference("gallery"); //tabla users
        botonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bitmapIMG != null){
                    nombreString= nombreEDT.getText().toString();
                    grupoString= grupoEDT.getText().toString();
                    pictureString=BitMapToString(bitmapIMG);

                    int posicionCal= -1;
                    int idGrupo= -1;

                    if(Globales.listaIdCalendarioFechaEspecif.size() == 0){
                        for(int j= 0; j< Globales.MisGrupos.size(); j++){
                            if(Globales.MisGrupos.get(j).getGroup_name().equals(grupoString)){
                                idGrupo= Globales.MisGrupos.get(j).getId_group();
                            }
                        }
                    }

                    for (int i= 0; i< Globales.listaIdCalendarioFechaEspecif.size(); i++){
                        for(int j= 0; j< Globales.MisGrupos.size(); j++){
                            if(Globales.MisGrupos.get(j).getGroup_name().equals(grupoString)){
                                idGrupo= Globales.MisGrupos.get(j).getId_group();
                            }
                            if(Globales.listaIdCalendarioFechaEspecif.get(i).getId_group() == Globales.MisGrupos.get(j).getId_group() &&
                                    Globales.MisGrupos.get(j).getGroup_name().equals(grupoString)){
                                posicionCal= i;
                                break;
                            }
                        }
                    }

                    if(posicionCal == -1){
                        Log.d("mensajejaja",String.valueOf(pictureString.length()));
                        Calendar_group_date nuevo= new Calendar_group_date(0,idGrupo, Globales.fechaGlobal);
                        insertarCalendarGroup(nuevo);
                    }
                    else{
                        Log.d("mensajejajaj",String.valueOf(pictureString.length()));
                        Gallery gal= new Gallery(0,nombreString,pictureString,
                                Globales.listaIdCalendarioFechaEspecif.get(posicionCal).getId_calendar_group_date(),
                                Globales.getUsuarioLogueado());
                        insertarEnBDGaleria(gal);
                    }
                }
            }
        });
        botonAgregarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });
        letra.addAll(Globales.nombreMisGrupos);
        ArrayAdapter adaptador= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, letra);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        grupoSpinner.setAdapter(adaptador);
        grupoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                grupoEDT.setText(letra.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }


    public void insertarCalendarGroup(Calendar_group_date nuevo){
        Log.d("fecha",nuevo.getCalendar_date());
        Log.d("calendar gropu date",String.valueOf(nuevo.getId_calendar_group_date()));
        Log.d("id grupo",String.valueOf(nuevo.getId_group()));

        Call<Integer> calendarioActividades=  conexionIP.getServidor().insertCalendarGroupDate(nuevo);
        calendarioActividades.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                String respuesta= response.message();
                Log.d("mensaje",respuesta);
                int idGrupo= response.body();
                Log.d("mensaje",String.valueOf(pictureString.length()));
                Gallery gal= new Gallery(0,nombreString,pictureString,idGrupo,Globales.getUsuarioLogueado());
                insertarEnBDGaleria(gal);
            }
            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });
    }


    private void openGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        /*if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            if (imageUri != null){
                try {
                    bitmapIMG = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    String ims= bitmapIMG.toString();

                    Log.d("cantidadadadad",String.valueOf(ims.length()));
                    Log.d("cantidadadadad",ims);
                    bitmapIMG= StringToBitMap(ims);
                    imagenCargada_imageView.setImageBitmap(bitmapIMG);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            bitmapIMG= imageBitmap;
            imagenCargada_imageView.setImageBitmap(bitmapIMG);
        }
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }



    public void insertarEnBDGaleria(Gallery gallery){
        String foto= BitMapToString(bitmapIMG);
        String id= databaseReference.push().getKey();
        gallery.setPicture(foto);
        databaseReference.child(id).setValue(gallery);
        Toast.makeText(getApplicationContext(), "Se ha creado el archivo exitosamente", Toast.LENGTH_SHORT).show();
        Intent nuevo = new Intent(getApplicationContext(), com.example.irinacubillovargas.calendar.GUI.Calendar.class);
        startActivity(nuevo);
        finish();
        /*
        Call<Boolean> calendarioActividades=  conexionIP.getServidor().insertGallery(gallery);
        calendarioActividades.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.d("cacacs",response.message());

                if(response.body().equals(true)){
                    Toast.makeText(getApplicationContext(), "Se ha creado el archivo exitosamente", Toast.LENGTH_SHORT).show();
                    Intent nuevo = new Intent(getApplicationContext(), com.example.irinacubillovargas.calendar.GUI.Calendar.class);
                    startActivity(nuevo);
                }
                else{
                    Toast.makeText(getApplicationContext(), "ERROR: no se ha creado el archivo", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });*/
    }

    public void vinvularElementos(){
        botonAceptar= (Button)findViewById(R.id.crearArchivo_botonAceptar);
        botonAgregarImagen= (Button)findViewById(R.id.crearArcvhivo_botonsubirFoto);
        nombreEDT= (EditText)findViewById(R.id.crearArchivo_nombre);
        grupoEDT= (EditText)findViewById(R.id.crearArchivo_grupo);
        imagenCargada_imageView= (ImageView)findViewById(R.id.crearArchivo_imageView);
        grupoSpinner= (Spinner)findViewById(R.id.crearArchivo_spinnerGrupo);
    }



}