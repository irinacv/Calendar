package com.example.irinacubillovargas.calendar.GUI;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.irinacubillovargas.calendar.Class.Events;
import com.example.irinacubillovargas.calendar.R;

import java.util.ArrayList;

public class AdapterEvento extends BaseAdapter {
    protected Activity activity;
    protected ArrayList<Events> items;

    public AdapterEvento (Activity activity, ArrayList<Events> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<Events> evento) {
        for (int i = 0; i < evento.size(); i++) {
            items.add(evento.get(i));
        }
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.lista_actividades_layout, null);
        }

        Events dir = items.get(position);

        TextView title = (TextView) v.findViewById(R.id.eventoTituloEvento);
        title.setText(dir.getName());

        TextView description = (TextView) v.findViewById(R.id.eventoDescripcionEvento);
        description.setText(dir.getDescription_event());

        return v;
    }
}
