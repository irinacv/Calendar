package com.example.irinacubillovargas.calendar.Class;

import java.util.ArrayList;
import java.util.List;

public class Globales {
    public static String fechaGlobal= "";
    public static String usuarioLogueado= "";
    public static List<User> listaUsuarios= new ArrayList<>();

    public static List<Calendar_group_date> listaIdCalendarioFechaEspecif= new ArrayList<>();

    public static List<Calendar_group_date>listaActividades= new ArrayList<>();
    public static List<String>nombreMisGrupos= new ArrayList<>();

    public static List<My_groups>MisGrupos= new ArrayList<>();







    public static List<String> getNombreMisGrupos() {
        return nombreMisGrupos;
    }

    public static void setNombreMisGrupos(List<String> nombreMisGrupos) {
        Globales.nombreMisGrupos = nombreMisGrupos;
    }

    public static String getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public static void setUsuarioLogueado(String usuarioLogueado) {
        Globales.usuarioLogueado = usuarioLogueado;
    }

    public static List<User> getListaUsuarios() {
        return listaUsuarios;
    }

    public static void setListaUsuarios(List<User> listaUsuarios) {
        Globales.listaUsuarios = listaUsuarios;
    }

    public static List<Calendar_group_date> getListaActividades() {
        return listaActividades;
    }

    public static void setListaActividades(List<Calendar_group_date> listaActividades) {
        Globales.listaActividades = listaActividades;
    }
}
