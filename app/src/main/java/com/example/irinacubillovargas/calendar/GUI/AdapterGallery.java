package com.example.irinacubillovargas.calendar.GUI;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.irinacubillovargas.calendar.Class.Events;
import com.example.irinacubillovargas.calendar.Class.Gallery;
import com.example.irinacubillovargas.calendar.R;

import java.util.ArrayList;

public class AdapterGallery extends BaseAdapter {


    private Context mContext;
    private Integer[] mThmbais={R.drawable.foto1,R.drawable.foto2,R.drawable.foto3,R.drawable.foto1,R.drawable.foto1,};
    ArrayList<Gallery>items;

    public AdapterGallery (Context activity, ArrayList<Gallery>galleryArrayList) {
        this.mContext= activity;
        this.items= galleryArrayList;
    }

    public int getCount() {
        return items.size();
    }

    public void clear() {

    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setLayoutParams(new GridView.LayoutParams(450,450));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(3,3,3,3);
        String bitString= items.get(position).getPicture();
        Bitmap bitmapIMG= StringToBitMap(bitString);
        imageView.setImageBitmap(bitmapIMG);
        //imageView.setImageResource(mThmbais[position]);
        return imageView;
    }

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }
}