package com.example.irinacubillovargas.calendar.GUI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CalendarView;
import android.widget.Toast;

import com.example.irinacubillovargas.calendar.Class.Calendar_group_date;
import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.Class.My_groups;
import com.example.irinacubillovargas.calendar.Conexion.IP;
import com.example.irinacubillovargas.calendar.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Calendar extends AppCompatActivity {
    private IP conexionIP = new IP();

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        CalendarView calendarView=(CalendarView) findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,int dayOfMonth) {
                String separacion1= "-";
                String separacion2= "-";
                month= month+1;
                if(month<10){
                    separacion1="-0";
                }
                if (dayOfMonth<10){
                    separacion2="-0";
                }
                String fecha= year+separacion1+month+separacion2+dayOfMonth;
                Globales.fechaGlobal= fecha;
                Globales.listaIdCalendarioFechaEspecif.clear();

                for (int i= 0; i< Globales.listaActividades.size(); i++){
                    if(Globales.listaActividades.get(i).getCalendar_date().equals(fecha)){
                        Globales.listaIdCalendarioFechaEspecif.add(Globales.listaActividades.get(i));
                        Log.d("hola 3", String.valueOf(Globales.listaIdCalendarioFechaEspecif.size()));
                    }
                }
                Intent pasar = new Intent(Calendar.this,Options.class);
                startActivity(pasar);
            }
        });
        obtenerMisGrupos();
        obtenerTodosLasActividades();
    }


    public void obtenerTodosLasActividades(){
        try{
            String user= Globales.usuarioLogueado;
            Call<List<Calendar_group_date>> calendarioActividades=  conexionIP.getServidor().obtenerActividadesCalendario(user);
            calendarioActividades.enqueue(new Callback<List<Calendar_group_date>>() {
                @Override
                public void onResponse(Call<List<Calendar_group_date>> call, Response<List<Calendar_group_date>> response) {
                    List<Calendar_group_date> lista= response.body();
                    Globales.setListaActividades(lista);
                    Log.d("Hola 1", String.valueOf(Globales.listaActividades.size()));
                    Log.d("Hola 2", String.valueOf(Globales.listaActividades.size()));
                }

                @Override
                public void onFailure(Call<List<Calendar_group_date>> call, Throwable t) {

                }
            });
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Eror de conexión", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_crearG) {
            Intent nuevo = new Intent(getApplicationContext(),CrearGrupo.class);
            startActivity(nuevo);
        }

        if (id == R.id.action_verG) {
            Intent nuevo = new Intent(getApplicationContext(),VerGrupos.class);
            startActivity(nuevo);
        }
        return super.onOptionsItemSelected(item);
    }

    public void obtenerMisGrupos() {
        try{
            String user= Globales.usuarioLogueado;
            Call<List<My_groups>> misGrupos=  conexionIP.getServidor().obtenerMisGrupos(user);
            misGrupos.enqueue(new Callback<List<My_groups>>() {
                @Override
                public void onResponse(Call<List<My_groups>> call, Response<List<My_groups>> response) {
                    List<My_groups>lista= response.body();
                    Globales.MisGrupos.clear();
                    Globales.nombreMisGrupos.clear();

                    Globales.MisGrupos.addAll(lista);

                    for (int i= 0; i<lista.size(); i++){
                        Globales.nombreMisGrupos.add(lista.get(i).getGroup_name());
                    }

                }
                @Override
                public void onFailure(Call<List<My_groups>> call, Throwable t) {
                }
            });
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Eror de conexión", Toast.LENGTH_LONG).show();
        }
    }

}
