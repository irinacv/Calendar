package com.example.irinacubillovargas.calendar.GUI;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.irinacubillovargas.calendar.Class.User;
import com.example.irinacubillovargas.calendar.R;

import java.util.ArrayList;

/**
 * Created by Irina Cubillo Vargas on 19/5/2018.
 */

public class AdapterUsuarios extends BaseAdapter {
    protected Activity activity;
    protected ArrayList<User> items;

    public AdapterUsuarios (Activity activity, ArrayList<User> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_user_group, null);
        }

        User dir = items.get(position);

        TextView title = (TextView) v.findViewById(R.id.nameUser);
        title.setText(dir.getUsername());

        return v;
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<User> evento) {
        for (int i = 0; i < evento.size(); i++) {
            items.add(evento.get(i));
        }
    }
}
