package com.example.irinacubillovargas.calendar.GUI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.irinacubillovargas.calendar.Class.Calendar_group_date;
import com.example.irinacubillovargas.calendar.Class.Events;
import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.Conexion.IP;
import com.example.irinacubillovargas.calendar.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrearEventoActivity extends AppCompatActivity {

    private IP conexionIP = new IP();
    Button botonCrearEvento;
    EditText hora,descripcion,nombre,grupo;
    Spinner grupoSpinner;
    String horaRealiz;
    String descripEve;
    String nombreEve;
    String grupoEve;

    ArrayList<String>listaNombreGrupos= new ArrayList<>();
    ArrayList<String> letra = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_evento);
        botonCrearEvento= (Button)findViewById(R.id.crearEvento_botonAceptar);
        hora=(EditText)findViewById(R.id.crearEvento_hora);
        descripcion=(EditText)findViewById(R.id.crearEvento_descripcion);
        nombre=(EditText)findViewById(R.id.crearEvento_nombre);
        grupo=(EditText)findViewById(R.id.crearEvento_grupo);

        grupoSpinner = (Spinner) findViewById(R.id.crearEvento_spinnerGrupo);
        letra.addAll(Globales.nombreMisGrupos);
        ArrayAdapter adaptador= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, letra);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        grupoSpinner.setAdapter(adaptador);

        grupoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                grupo.setText(letra.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        botonCrearEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                horaRealiz= hora.getText().toString();
                descripEve= descripcion.getText().toString();
                nombreEve=nombre.getText().toString();
                grupoEve=grupo.getText().toString();

                int posicionCal= -1;
                int idGrupo= -1;

                if(Globales.listaIdCalendarioFechaEspecif.size() == 0){
                    for(int j= 0; j< Globales.MisGrupos.size(); j++){
                        if(Globales.MisGrupos.get(j).getGroup_name().equals(grupoEve)){
                            idGrupo= Globales.MisGrupos.get(j).getId_group();
                        }
                    }
                }

                for (int i= 0; i< Globales.listaIdCalendarioFechaEspecif.size(); i++){
                    for(int j= 0; j< Globales.MisGrupos.size(); j++){
                        if(Globales.MisGrupos.get(j).getGroup_name().equals(grupoEve)){
                            idGrupo= Globales.MisGrupos.get(j).getId_group();
                        }
                        if(Globales.listaIdCalendarioFechaEspecif.get(i).getId_group() == Globales.MisGrupos.get(j).getId_group() &&
                                Globales.MisGrupos.get(j).getGroup_name().equals(grupoEve)){
                            posicionCal= i;
                            break;
                        }
                    }
                }

                if(posicionCal == -1){
                    String fehcaCrea= Globales.fechaGlobal;
                    Log.d("dasdas 22", "dsadasdasdas");
                    Calendar_group_date nuevo= new Calendar_group_date(0,idGrupo, Globales.fechaGlobal);
                    insertarCalendarGroup(nuevo);
                }
                else{
                    Log.d("dasdas 22", String.valueOf(idGrupo));
                    Log.d("dasdas ee", String.valueOf(posicionCal));
                    Events evento= new Events(0,nombreEve,descripEve,horaRealiz,
                            Globales.listaIdCalendarioFechaEspecif.get(posicionCal).getId_calendar_group_date(),
                            Globales.getUsuarioLogueado());
                    insertarEnBDEvento(evento);
                }
            }
        });
    }

    public void insertarCalendarGroup(Calendar_group_date nuevo){
        Log.d("fecha",nuevo.getCalendar_date());
        Log.d("calendar gropu date",String.valueOf(nuevo.getId_calendar_group_date()));
        Log.d("id grupo",String.valueOf(nuevo.getId_group()));

        Call<Integer> calendarioActividades=  conexionIP.getServidor().insertCalendarGroupDate(nuevo);
        calendarioActividades.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                String respuesta= response.message();
                Log.d("mensaje",respuesta);
                int idGrupo= response.body();
                Events evento= new Events(0,nombreEve,descripEve,horaRealiz,idGrupo,Globales.getUsuarioLogueado());
                insertarEnBDEvento(evento);
            }
            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });
    }

    public void insertarEnBDEvento(Events evento){
        Call<Boolean> calendarioActividades=  conexionIP.getServidor().insertarEvento(evento);
        calendarioActividades.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.d("cacacs",response.message());
                if(response.body().equals(true)){
                    Toast.makeText(CrearEventoActivity.this, "Se ha creado el evento exitosamente", Toast.LENGTH_SHORT).show();
                    Intent nuevo = new Intent(getApplicationContext(), com.example.irinacubillovargas.calendar.GUI.Calendar.class);
                    startActivity(nuevo);
                    finish();
                }
                else{
                    Toast.makeText(CrearEventoActivity.this, "Error: no se ha creado evento", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }
}
