package com.example.irinacubillovargas.calendar.GUI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.irinacubillovargas.calendar.Class.Globales;
import com.example.irinacubillovargas.calendar.Class.User;
import com.example.irinacubillovargas.calendar.Conexion.IP;
import com.example.irinacubillovargas.calendar.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    Button login;
    Button registro;
    private IP conexionIP = new IP();
    EditText nombreU, passU;
    String username, pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        nombreU= (EditText)findViewById(R.id.input_usuario);
        passU=(EditText)findViewById(R.id.input_contrasena);
        login= (Button)findViewById(R.id.boton_iniciar_sesion);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loguear();
                //Intent nuevo = new Intent(getApplicationContext(),Calendar.class);
                //startActivity(nuevo);
            }
        });
        registro=(Button)findViewById(R.id.registrosB);
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registrar= new Intent(getApplicationContext(),Register.class);
                startActivity(registrar);
            }
        });
    }

    public void loguear(){
        //Intent nuevo = new Intent(getApplicationContext(),Calendar.class);
        //startActivity(nuevo);
        try{
            username= nombreU.getText().toString().trim();
            pass= passU.getText().toString().trim();
            Call<List<User>> usuarios=  conexionIP.getServidor().obtenerListaDeUsuarios();
            usuarios.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    List<User> lista= response.body();
                    boolean siSeEncuentra= false;
                    for (int i = 0; i < lista.size(); i++)
                    {
                        if (lista.get(i).getUsername().equals(username) && lista.get(i).getUserpass().equals(pass)){
                            siSeEncuentra= true;
                            Globales.setUsuarioLogueado(username);
                            Globales.setListaUsuarios(lista);
                            //Toast.makeText(getApplicationContext(), Globales.getListaUsuarios().toString(), Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }

                    if(siSeEncuentra && !Globales.usuarioLogueado.isEmpty()){
                        Toast.makeText(getApplicationContext(),"si esta logueado", Toast.LENGTH_LONG).show();

                        Intent nuevo = new Intent(getApplicationContext(),Calendar.class);
                        startActivity(nuevo);
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Error no coinciden los credenciales con ningún usuario", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {

                }
            });
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Eror de conexión", Toast.LENGTH_LONG).show();
        }
    }
}
